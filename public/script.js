
const tuSiteConf = {
    theme: { dark: "theme-dark", light: "theme-light" },
}

document.body.className = (localStorage["tusite_theme"] ??= tuSiteConf.theme.dark)
fetch("./data-fr.json").then(d=>d.json()).then(siteData=>{
    new Vue({ el: '#histo-timeline', data: { eventDates: siteData.events } })
    new Vue({ el: '#web-projects', data: { sites: siteData.webAppProjects } })
    new Vue({ el: '#menu', data: { isLightTheme: localStorage["tusite_theme"] == tuSiteConf.theme.light }, methods: {setDarkTheme () { document.body.className = localStorage["tusite_theme"] = this.isLightTheme ? tuSiteConf.theme.light : tuSiteConf.theme.dark }} })
    fetch(`https://gitlab.com/api/v4/groups/techunit/projects?include_subgroups=true`).then(d=>d.json()).then(projects =>
        fetch("https://gitlab.com/api/v4/groups/techunit/subgroups").then(d=>d.json()).then(groups => 
            new Vue({ 
                el: '#git-projects', 
                data: { 
                    groups: groups.map(t=>{return{
                        text:t.name, 
                        url: t.web_url,
                        full_path: t.full_path,
                        description: t.description, 
                        projects: projects.filter(p=>p.namespace.full_path == t.full_path).map(p=>{return{url: p.web_url, name: p.name}}),
                        image: t.avatar_url || "https://assets.gitlab-static.net/uploads/-/system/group/avatar/12505232/logo-tu-v1-org.png",
                    }}) 
                } 
            })
        )
    )
})